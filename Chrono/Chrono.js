
let milliseconde = 0;
let seconde = 0;
let minute = 0;


document.getElementById("Start").addEventListener("click", Play, false);
document.getElementById("Pause").addEventListener("click", Stop, false);
document.getElementById("Reset").addEventListener("click", Reset, false);



function Play() {

    myVar = setInterval(Incremente, 10);
    document.getElementById("Start").disabled = true;

}

function Incremente() {
    milliseconde++;

    affichage();

    if (milliseconde == 100) {
        milliseconde = 0;
        seconde++;
    }

    if (seconde == 60) {
        minute++;
        seconde = 0;
    }


}

function Stop() {

    clearInterval(myVar);
    document.getElementById("Start").disabled = false;
}

function Reset() {
    milliseconde = 0;
    seconde = 0;
    minute = 0;

    affichage();


    document.getElementById("Start").disabled = false;
    clearInterval(myVar);
}


function affichage() {

    document.getElementById("minutes").innerText = minute;
    document.getElementById("seconde").innerText = seconde;
    document.getElementById("mili").innerText = milliseconde;

}

